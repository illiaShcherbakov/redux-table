const initialState = {
  showForm: false,
  inputValues: {
    id: '',
    name: '',
    department: '',
    status: 'green'
  },
};

const form = (state = initialState, action) => {

  switch (action.type) {
    case 'SHOW_FORM':
      if ( state.showForm ) {
        return Object.assign({}, state, {
          showForm: false,
        })
      } else {
        if ( action.studentData == null ) {
          return Object.assign({}, state, {
            showForm: true,
            inputValues: {
              id: '',
              name: '',
              department: '',
              status: 'green'
            }
          })
        }
        return Object.assign({}, state, {
          showForm: true,
          inputValues: {
            id: action.studentData.id,
            name: action.studentData.name,
            department: action.studentData.department,
            status: action.studentData.status
          }
        });
      }

    case 'HIDE_FORM':
      return Object.assign({}, state, {
        showForm: false
      });
  }
  return state;
};


export default form;
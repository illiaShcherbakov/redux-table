
const initialState = {
  students: [
    {
      id: '00001',
      name: 'John Doe',
      department: 'Department of History of Art',
      status: 'green'
    },
    {
      id: '00002',
      name: 'Satan',
      department: 'Department of Sociology',
      status: 'yellow'
    },
    {
      id: '00003',
      name: 'Hideo Kojima',
      department: 'Department of Anglo-Saxon, Norse and Celtic',
      status: 'red'
    }
  ]
};

const table = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD':
      let {id, name, department, status} = action.newStudent;
      const newStudArray = state.students.concat([{id, name, department, status}]);
      return Object.assign({}, state, {
        students: newStudArray
      });
    case 'EDIT':
      var {id, name, department, status} = action.editStudentData;
      var newStudArray = state.students.map(function(el) {
        if ( el.id === id ) {
          el.name = name;
          el.department = department;
          el.status = status;
        }
        return el;
      });
      return Object.assign({}, state, {
        students: newStudArray
      });
  }

  return state;
};


export default table;
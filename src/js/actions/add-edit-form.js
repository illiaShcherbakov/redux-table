export function displayForm(obj) {
  return {
    type: 'SHOW_FORM',
    studentData: obj,
  }
}

export function hideForm() {
  return {
    type: 'HIDE_FORM'
  }
}

export function submitAdd(obj) {
  return {
    type: 'ADD',
    newStudent: obj
  }
}

export function submitEdit(obj) {
  return {
    type: 'EDIT',
    editStudentData: obj
  }
}
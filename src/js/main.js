import React from 'react';
import ReactDOM from 'react-dom';

import App from 'containers/App';
import RootContainer from 'containers/RootContainer';
import Login from 'containers/Login';

import { Provider } from 'react-redux';
import { createStore } from 'redux';

import {Router, Route, browserHistory, IndexRoute } from 'react-router';

import rootReducer from './reducers/main';

const store = createStore(rootReducer);
const onDefault = localStorage.getItem( 'user' ) ? RootContainer : Login;


ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={onDefault}/>
        <Route path="/students" component={RootContainer} />
        <Route path="/login" component={Login} />
      </Route>
    </Router>
  </Provider>, document.getElementById("app"));


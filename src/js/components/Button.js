import React from 'react';

const Button = (props) => {

  const handleClick = (e) => {
    e.preventDefault();
    props.displayForm(props.data);
  };

  return (
    <button className={props.classes} onClick={handleClick}>{props.textValue}</button>
  )
};

export default Button;
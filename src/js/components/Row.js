import React from 'react';
import Button from './Button';

const Row = props => {

  const obj = {
    id: props.nodeId,
    name: props.name,
    department: props.department,
    status: props.status,
  };

  if ( props.status == 'green' ) {
    var status = <div className="status-box green"></div>
  } else if ( props.status == 'yellow' ) {
    var status = <div className="status-box yellow"></div>
  } else if ( props.status == 'red') {
    var status = <div className="status-box red"></div>
  }

  return (
    <tr>
      <th>{props.nodeId}</th>
      <th>{props.name}</th>
      <th>{props.department}</th>
      <th>{status}</th>
      <th>
        <Button data={obj} displayForm={props.displayForm} classes="edit btn btn-primary" textValue="Edit"/>


      </th>
    </tr>
  )
};

export default Row;
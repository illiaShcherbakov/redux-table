import React from 'react';
import { browserHistory } from 'react-router';

export default class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      errorEmail: false
    }
  }

  componentWillMount() {
    if ( localStorage.getItem( 'user' ) ) {
      browserHistory.push('/students');
    }
  }

  handleChange(e) {
    let field = e.target.id;
    this.setState({
      [field]: e.target.value
    });
  }

  handleClick(e) {
    e.preventDefault();
    const re = /\S+@\S+\.\S+/;

    if ( re.test(this.state.email) ) {
      const obj = {
        email: this.state.email,
        password: this.state.password
      };

      localStorage.setItem( 'user', JSON.stringify(obj) );
      browserHistory.push('/students');
    } else {
      console.log('wrong email');
      this.setState({
        errorEmail: true
      })
    }
  }

  handleFocus(e) {
    console.log('focus');
    this.setState({
      errorEmail: false
    })
  }

  toggleErrorClass(classString, errorState) {
    console.log('toggle class');
    return classString + ' ' + ((errorState) ? 'has-error' : 'correct' );
  }

  render() {

    return (
      <div className="well login">
        <div className="form login-form">
          <div className={this.toggleErrorClass('form-group', this.state.errorEmail)}>
            <input
              type="email"
              id="email"
              value={this.state.email}
              onChange={(e) => this.handleChange(e)}
              name="email"
              placeholder="Email"
              className="form-control"
              onFocus={(e) => this.handleFocus(e)}
            />
          </div>
          <div className="form-group">
            <input
              type="password"
              id="password"
              value={this.state.password}
              onChange={(e) => this.handleChange(e)}
              name="email"
              placeholder="Password"
              className="form-control"/>
          </div>
          <div className="form-group clearfix">
            <input type="submit" className="btn btn-primary pull-right" value="Log In" onClick={(e) => this.handleClick(e)} />
          </div>

        </div>
      </div>
    )
  }
}
import React from 'react';
import { connect } from 'react-redux';
import Row from '../components/Row';


//Actions
import {displayForm} from '../actions/add-edit-form';

class Table extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var listNodes = this.props.students.map( listItem => {
      return (
        <Row key={listItem.id} nodeId={listItem.id} name={listItem.name}  department={listItem.department} status={listItem.status} displayForm={this.props.displayForm} />
      )
    });


    return (
        <table className="table table-striped table-hover" >
          <thead>
            <tr>
              <th>id</th>
              <th>Name</th>
              <th>Department</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {listNodes}
          </tbody>
        </table>
    )
  }
}

//console.log(state);
//
const mapStateToProps = (state) => {
  return {
    students: state.table.students,
  }
};

export default connect(mapStateToProps, {displayForm})(Table);
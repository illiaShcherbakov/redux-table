import React from 'react';
import { connect } from 'react-redux';
import Button from '../components/Button';

import {displayForm} from '../actions/add-edit-form';
import {hideForm} from '../actions/add-edit-form';
import {submitAdd}from '../actions/add-edit-form';
import {submitEdit} from '../actions/add-edit-form';

class AddEditForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      department: '',
      status: '',
      isVisible: null
    }
  }

  generateId() {
    return Math.floor(Math.random()*90000) + 10000;
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      name: nextProps.inputValues.name,
      department: nextProps.inputValues.department,
      status: nextProps.inputValues.status,
      isVisible: nextProps.showForm
    })
  }

  handleChange(e) {
    let field = e.target.id;
    this.setState({
      [field]: e.target.value
    });
  }

  handleAdd(e) {
    e.preventDefault();
    const obj = {
      id: this.generateId().toString(),
      name: this.state.name,
      department: this.state.department,
      status: this.state.status
    };
    this.props.submitAdd(obj);
    this.props.displayForm();
    //this.props.hideForm();
  }
  handleEdit(e) {
    e.preventDefault();
    const obj = {
      id: this.props.inputValues.id,
      name: this.state.name,
      department: this.state.department,
      status: this.state.status
    };
    this.props.submitEdit(obj);
    this.props.displayForm();
  }

  handleClose(e) {
    e.preventDefault();
    this.props.hide();
  }


  render() {
    let classes= 'clearfix addEdit-form';
    if ( this.state.isVisible ) {
      classes = classes + ' show';
    }

    return (
      <div className={classes}>
        <form>
          <div className="row close-wrapper">
            <div className="col-md-12">
              <Button data={null} displayForm={this.props.displayForm} classes="close" textValue="&#x2716;" />
            </div>

          </div>
          <div className="form-group clearfix">
            <label htmlFor="name" className="col-md-2 control-label">Name</label>
            <div className="col-md-10">
              <input type="text" id="name" className="form-control" value={this.state.name} onChange={(e) => this.handleChange(e)} placeholder="Write the name of the student"/>
            </div>
          </div>
          <div className="form-group clearfix">
            <label htmlFor="department" className="col-md-2 control-label">Department</label>
            <div className="col-md-10">
              <input type="text" id="department" className="form-control" value={this.state.department} onChange={(e) => this.handleChange(e)} placeholder="Write the name of the department"/>
            </div>
          </div>
          <div className="form-group clearfix">
            <label htmlFor="status" className="col-md-2 control-label">Status</label>
            <div className="col-md-10">
              <select className="form-control" name="status" id="status" value={this.state.status} onChange={(e) => this.handleChange(e)}>
                <option value="green">Green</option>
                <option value="yellow">Yellow</option>
                <option value="red">Red</option>
              </select>
            </div>
          </div>
          <div className="row">
            <div className="col-md-10 col-md-offset-2 text-right btn-wrapper">
              { this.props.inputValues.id.length ? (
                  <input type="submit" value="Save" className="btn btn-primary pull-right edit" onClick={(e) => this.handleEdit(e)} disabled={!this.state.name || !this.state.department} />
                ) : (
                  <input type="submit" value="Save" className="btn btn-primary pull-right add" onClick={(e) => this.handleAdd(e)} disabled={!this.state.name || !this.state.department} />
                )
              }
            </div>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    showForm: state.form.showForm,
    inputValues: state.form.inputValues,
    goal: state.form.goal
  }
};


export default connect(mapStateToProps, {displayForm, submitAdd, submitEdit, hideForm})(AddEditForm);
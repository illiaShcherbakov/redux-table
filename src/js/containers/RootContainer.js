import React from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import Table from './Table';
import AddEditForm from './AddEditForm';
import Button from '../components/Button';

//Actions
import {displayForm} from '../actions/add-edit-form';
import {hideForm} from '../actions/add-edit-form';
import {submitAdd}from '../actions/add-edit-form';
import {submitEdit} from '../actions/add-edit-form';

class RootContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    if ( !localStorage.getItem( 'user' ) ) {
      browserHistory.push('/login');
    }
  }

  handleClick(e) {
    this.props.displayForm(null, 'add');
  }

//<button className="add btn btn-primary" onClick={(e) => this.handleClick(e)}>Add</button>
  render() {
    return (
      <div className="well">
        <Button data={null} displayForm={this.props.displayForm} classes="add btn btn-primary" textValue="Add" />
        <AddEditForm
          isVisible={this.props.showForm}
          inputValues={this.props.inputValues}
          goal={this.props.goal}
          add={this.props.submitAdd}
          hide={this.props.hideForm}
          edit={this.props.submitEdit}
        />
        <Table/>
      </div>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    showForm: state.form.showForm,
    inputValues: state.form.inputValues,
    goal: state.form.goal
  }
};

export default connect(mapStateToProps, {displayForm, submitAdd, hideForm, submitEdit})(RootContainer);